<?php

$info = array(

  // Pre-defined color schemes
  'schemes' => array(
    '#ffffff,#3e4967,#668040,#485a2d,#000000' => t('Green (Default)'),
    '#ffffff,#3e4967,#7b0f0f,#560b0b,#000000' => t('Red'),
  ),

  // Images to copy over
  'copy' => array(
    'images/bullet.gif',
    'images/search.gif',
    'images/bullet_minus.png',
    'images/bullet_plus.png',
    'images/quote.png',
    'images/bg.gif',
    'images/bg2.gif',
  ),
);
